package main

// YoinkDocument represents the structure of documents from yoinker with vectors at metadata level
type YoinkDocument struct {
	Source              string                 `json:"source"`
	Timestamp           string                 `json:"timestamp"`
	GUID                string                 `json:"guid"`
	SHA256              string                 `json:"sha256"`
	Type                string                 `json:"type"`
	Data                map[string]interface{} `json:"data"`
	RunID               string                 `json:"run_id"`
	CollectionID        string                 `json:"collection_id"`
	ContentMod26Bigram  []float64             `json:"content_mod26_bigram"`
	PathMod26Bigram     []float64             `json:"path_mod26_bigram"`
	PathMod26Trigram    []float64             `json:"path_mod26_trigram"`
	ContentMod26Trigram []float64             `json:"content_mod26_trigram"`
}

// SearchResult with score for sorting
type VectorSearchResult struct {
	Document YoinkDocument
	Score    float64
}

const indexDirectory = "search_indices"