package main

import (
    "fmt"
    "sync"
	"encoding/json"
	"path/filepath"
	"time"
	"log"
	"container/heap"
)

var (
    bigramGrid  *SparseGrid
    trigramGrid *SparseGrid
    gridMutex   sync.RWMutex
)

func initGrids() error {
    log.Printf("Starting grid initialization...")
    gridMutex.Lock()
    defer gridMutex.Unlock()

    log.Printf("Creating bigram grid (26x26 dimensions)...")
    bigramGrid = NewSparseGrid(676, 0.1)    // 26x26 dimensions
    
    log.Printf("Creating trigram grid (26x26x26 dimensions)...")
    trigramGrid = NewSparseGrid(17576, 0.1) // 26x26x26 dimensions

	return nil

    if kvStore == nil {
        log.Printf("KV store is nil, skipping grid population")
        return nil
    }
    
    var docsProcessed, bigramsInserted, trigramsInserted int
    lastProgress := time.Now()
    
    log.Printf("Starting to populate grids from KV store...")
    _, err := kvStore.MapFunc(func(key, value []byte) error {

        docsProcessed++
        if time.Since(lastProgress) > time.Second*2 {
            log.Printf("Grid population progress: processed %d documents, inserted %d bigrams, %d trigrams",
                docsProcessed, bigramsInserted, trigramsInserted)
            lastProgress = time.Now()
        }
        
        var doc YoinkDocument
        if err := json.Unmarshal(value, &doc); err != nil {
            log.Printf("Error unmarshaling document %s: %v", string(key), err)
            return nil
        }
        
        if len(doc.ContentMod26Bigram) > 0 {
            bigramGrid.Insert(Point{
                Coords: doc.ContentMod26Bigram,
                ID:     doc.GUID,
            })
            bigramsInserted++
        }
        
        if len(doc.ContentMod26Trigram) > 0 {
            trigramGrid.Insert(Point{
                Coords: doc.ContentMod26Trigram,
                ID:     doc.GUID,
            })
            trigramsInserted++
        }
        return nil
    })
    
    log.Printf("Grid initialization complete: processed %d documents, inserted %d bigrams, %d trigrams",
        docsProcessed, bigramsInserted, trigramsInserted)
    
    if err != nil {
        log.Printf("Error during grid initialization: %v", err)
        return err
    }
    
    return nil
}

 type scoredDoc struct {
	docID string
	score float64
}

func gridSearch(query string, limit int) ([]ResultsData, error) {
    gridMutex.RLock()
    defer gridMutex.RUnlock()



    if bigramGrid == nil || trigramGrid == nil {
        return nil, fmt.Errorf("grids not initialized")
    }

    log.Printf("Starting grid search for query: %s", query)
    queryBytes := []byte(query)
    queryBigrams, err := Mod26Vector(queryBytes)
    if err != nil {
        return nil, err
    }

    queryTrigrams, err := trigramVector(queryBytes)
    if err != nil {
        return nil, err
    }

    // Create min-heap to keep only top N results
    topResults := &minHeap{}
    heap.Init(topResults)
    processedDocs := make(map[string]bool)
    maxHeapSize := limit * 2 // Keep a few extra for safety

    // Process bigram matches
    bigramPoint := Point{Coords: queryBigrams}
    log.Printf("Processing bigram neighbors...")
    for _, neighbor := range bigramGrid.GetNeighbors(bigramPoint, 0.3, limit) {
        if processedDocs[neighbor.ID] {
            continue
        }
        processedDocs[neighbor.ID] = true

        score := cosineSimilarity(queryBigrams, neighbor.Coords)
        
        // Only store if it's potentially in our top results
        if topResults.Len() < maxHeapSize {
            heap.Push(topResults, scoredDoc{docID: neighbor.ID, score: score})
        } else if score > topResults.Peek().score {
            heap.Pop(topResults)
            heap.Push(topResults, scoredDoc{docID: neighbor.ID, score: score})
        }
    }

    // Process trigram matches
    trigramPoint := Point{Coords: queryTrigrams}
    log.Printf("Processing trigram neighbors...")
    for _, neighbor := range trigramGrid.GetNeighbors(trigramPoint, 0.3, limit) {
        if processedDocs[neighbor.ID] {
            continue
        }
        processedDocs[neighbor.ID] = true

        score := cosineSimilarity(queryTrigrams, neighbor.Coords) * 1.5 // Weight trigrams higher
        
        if topResults.Len() < maxHeapSize {
            heap.Push(topResults, scoredDoc{docID: neighbor.ID, score: score})
        } else if score > topResults.Peek().score {
            heap.Pop(topResults)
            heap.Push(topResults, scoredDoc{docID: neighbor.ID, score: score})
        }
    }

    // Convert heap to sorted results
    resultCount := min(topResults.Len(), limit)
    results := make([]scoredDoc, resultCount)
    for i := resultCount - 1; i >= 0; i-- {
        results[i] = heap.Pop(topResults).(scoredDoc)
    }

    log.Printf("Converting top %d results to final format...", len(results))
    searchResults := make([]ResultsData, 0, len(results))
    for _, r := range results {

        data, err := kvStore.Get([]byte(r.docID))
        if err != nil {
            continue
        }

        var doc YoinkDocument
        if err := json.Unmarshal(data, &doc); err != nil {
            continue
        }

        searchResults = append(searchResults, ResultsData{
            Id:       doc.GUID,
            DocId:    doc.GUID,
            URL:      "file://" + doc.Source,
            Title:    filepath.Base(doc.Source),
            Text:     fmt.Sprintf("Score: %.2f - Type: %s", r.score, doc.Type),
            Path:     doc.Source,
            FullText: fmt.Sprintf("%v", doc.Data["content"]),
            Class:    "Document",
        })
    }

    return searchResults, nil
}

// Support type for min-heap
type minHeap []scoredDoc

func (h minHeap) Len() int { return len(h) }
func (h minHeap) Less(i, j int) bool { return h[i].score < h[j].score }
func (h minHeap) Swap(i, j int) { h[i], h[j] = h[j], h[i] }
func (h *minHeap) Push(x interface{}) { *h = append(*h, x.(scoredDoc)) }
func (h *minHeap) Pop() interface{} {
    old := *h
    n := len(old)
    x := old[n-1]
    *h = old[0 : n-1]
    return x
}
func (h *minHeap) Peek() scoredDoc { return (*h)[0] }

// Helper function for min
func min(a, b int) int {
    if a < b {
        return a
    }
    return b
}
