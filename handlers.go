package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"sort"
	"strconv"
	"strings"
	"text/template"
	"time"

	"github.com/donomii/ensemblekv"
	"gitlab.com/donomii/TagExplorer/vectorstore"
	sweaviate "gitlab.com/donomii/weaviate-client"

	"context"
	"github.com/weaviate/weaviate-go-client/v4/weaviate"
	"github.com/weaviate/weaviate-go-client/v4/weaviate/auth"
	"github.com/weaviate/weaviate-go-client/v4/weaviate/graphql"
)

var reindexAtStartup bool

func displayHandler(w http.ResponseWriter, r *http.Request) {
	query := r.URL.Query().Get("id")
	if query == "" {
		log.Printf("No doc id provided")
		http.Redirect(w, r, "/", http.StatusSeeOther)
		return
	}

	var result ResultsData

	//Convert query to GUID FIXME

	// Try vector store first
	if kvStore != nil {
		data, err := kvStore.Get([]byte(query))
		if err == nil {
			var doc YoinkDocument
			if err := json.Unmarshal(data, &doc); err == nil {
				result = ResultsData{
					Id:       doc.GUID,
					DocId:    doc.GUID,
					URL:      "file://" + doc.Source,
					Title:    filepath.Base(doc.Source),
					Text:     fmt.Sprintf("Type: %s", doc.Type),
					Path:     doc.Source,
					FullText: fmt.Sprintf("%v", doc.Data["content"]),
					Class:    determineDocumentClass(doc.Type),
				}
			}
		}
	}

	// Fallback to Weaviate if not found in vector store
	if result.Id == "" {
		obj := sweaviate.GetObject(query)
		result = convertObject(obj)
	}

	// Handle display based on the document class and URL
	if strings.HasPrefix(result.URL, "file://") {
		filePath := strings.TrimPrefix(result.Path, "file://")
		http.ServeFile(w, r, filePath)
	} else {
		w.Write([]byte(result.FullText))
	}
}

func downloadObjectHander(w http.ResponseWriter, r *http.Request) {
	query := r.URL.Query().Get("id")
	if query == "" {
		log.Printf("No doc id provided")
		http.Redirect(w, r, "/", http.StatusSeeOther)
		return
	}

	var result ResultsData

	// Try vector store first
	if kvStore != nil {
		data, err := kvStore.Get([]byte(query))
		if err == nil {
			var doc YoinkDocument
			if err := json.Unmarshal(data, &doc); err == nil {
				result = ResultsData{
					Id:       doc.GUID,
					DocId:    doc.GUID,
					Path:     doc.Source,
					FullText: fmt.Sprintf("%v", doc.Data["content"]),
				}
			}
		}
	}

	// Fallback to Weaviate if not found in vector store
	if result.Id == "" {
		obj := sweaviate.GetObject(query)
		result = convertObject(obj)
	}

	data := []byte(result.FullText)
	filename := filepath.Base(result.Path)

	w.Header().Set("Content-Disposition", "attachment; filename="+filename)
	w.Write(data)
}

// determineDocumentClass maps document types to frontend classes
func determineDocumentClass(docType string) string {
	switch docType {
	case "code", "go", "python", "javascript", "typescript":
		return "Code"
	case "pdf", "doc", "docx", "txt":
		return "Files"
	case "email", "eml":
		return "Email"
	case "webpage", "html":
		return "Page"
	default:
		return "Document"
	}
}

func taskManagerHandler(w http.ResponseWriter, r *http.Request) {
	tmpl := template.Must(template.ParseFS(templates, "task-manager.html"))
	tmpl.Execute(w, nil)
}

func weaviateSearch(query string, mode string) (TemplateSearchResults, error) {
	q := strings.Split(query, " ")
	classes := sweaviate.GetClasses()
	var multiResult [][]map[string]string

	for _, class := range classes {
		var results []map[string]string
		switch mode {
		case "keyword":
			results = sweaviate.KeywordPropResults(q, class, 50)
		case "vector":
			results = sweaviate.VectorPropResults(q, class, 50)
		default: // "hybrid" or empty
			results = sweaviate.HybridPropResults(q, class, 50)
		}
		for _, result := range results {
			result["Class"] = class
		}
		multiResult = append(multiResult, results)
	}

	mergedResults := mergeResults(multiResult)
	templateResults := convertResults(mergedResults, 240)
	templateResults.Query = query

	return templateResults, nil
}

// Helper function to safely extract string values
func getStringValue(v interface{}) string {
	if v == nil {
		return ""
	}
	if s, ok := v.(string); ok {
		return s
	}
	return fmt.Sprintf("%v", v)
}

// Helper function to extract score from text
func extractScore(text string) float64 {
	parts := strings.Split(text, " ")
	for i, part := range parts {
		if part == "Score:" && i+1 < len(parts) {
			score, err := strconv.ParseFloat(strings.TrimRight(parts[i+1], "-"), 64)
			if err == nil {
				return score
			}
		}
	}
	return 0
}

// wcdSearch handles searching using the Weaviate Cloud client
func wcdSearch(query string, limit int) ([]ResultsData, error) {
    if os.Getenv("WCD_HOSTNAME") == "" {
        fmt.Println("WCD_HOSTNAME environment variable not set")
        return nil, fmt.Errorf("WCD_HOSTNAME environment variable not set")
    }
    if os.Getenv("WCD_API_KEY") == "" {
        fmt.Println("WCD_API_KEY environment variable not set")
        return nil, fmt.Errorf("WCD_API_KEY environment variable not set")
    }
    if os.Getenv("COHERE_APIKEY") == "" {
        fmt.Println("COHERE_APIKEY environment variable not set")
        return nil, fmt.Errorf("COHERE_APIKEY environment variable not set")
    }
	cfg := weaviate.Config{
		Host:       os.Getenv("WCD_HOSTNAME"),
		Scheme:     "https",
		AuthConfig: auth.ApiKey{Value: os.Getenv("WCD_API_KEY")},
		Headers: map[string]string{
			"X-Cohere-Api-Key": os.Getenv("COHERE_APIKEY"),
		},
	}

	client, err := weaviate.NewClient(cfg)
	if err != nil {
		return nil, fmt.Errorf("failed to create WCD client: %v", err)
	}

	ctx := context.Background()
	classes := []string{"Wikipedia"} // FIXME Add all your classes here
	var allResults []ResultsData

	for _, className := range classes {
		q := client.GraphQL().Get().
			WithClassName(className).
			WithFields(
				graphql.Field{Name: "title"},
				graphql.Field{Name: "text"},
				//graphql.Field{Name: "url"},
				//graphql.Field{Name: "path"},
				//graphql.Field{Name: "source"},
				graphql.Field{Name: "_additional { id score }"},
			).
			WithHybrid(client.GraphQL().HybridArgumentBuilder().WithQuery(query)).
			WithLimit(limit)

		result, err := q.Do(ctx)
		if err != nil {
			log.Printf("Error searching %s class: %v", className, err)
			continue
		}

        for _, err := range result.Errors {
            fmt.Printf("Error: %+v\n", err)
        }

        data := result.Data
        fmt.Printf("Data: %+v\n", data)
        get := data["Get"].(map[string]interface{})
        wikipedia := get["Wikipedia"]
        fmt.Printf("Wikipedia: %+v\n", wikipedia)
		// Process results
		for _, item := range wikipedia.([]interface{}) {
			if data, ok := item.(map[string]interface{}); ok {
                fmt.Printf("Data: %+v\n", item)


				var id string
				var score float64
				additional_i, ok := data["_additional"]
				if ok {
					additional := additional_i.(map[string]interface{})
					id = additional["id"].(string)
					score_str := additional["score"].(string)
                    score, _ = strconv.ParseFloat(score_str, 64)
				}

				// Map fields based on class type
				title := getStringValue(data["title"])
				text := getStringValue(data["text"])
				url := getStringValue(data["url"])
				path := getStringValue(data["path"])
				source := getStringValue(data["source"])

				// Create ResultsData
				rd := ResultsData{
					Id:       id,
					DocId:    id,
					URL:      url,
					Title:    title,
					Text:     fmt.Sprintf("Score: %.2f - %s", score, text),
					Path:     path,
					FullText: source,
					Class:    className,
				}

				allResults = append(allResults, rd)
			}
		}
	}

	// Sort results by score
	sort.Slice(allResults, func(i, j int) bool {
		scoreI := extractScore(allResults[i].Text)
		scoreJ := extractScore(allResults[j].Text)
		return scoreI > scoreJ
	})

	return allResults, nil
}

func searchHandler(w http.ResponseWriter, r *http.Request) {
	query := r.URL.Query().Get("q")
	searchType := r.URL.Query().Get("type") // weaviate, ball, grid, kv, wcd
	searchMode := r.URL.Query().Get("mode") // hybrid, keyword, or vector

	if query == "" {
		http.Redirect(w, r, "/", http.StatusSeeOther)
		return
	}

	var searchResults []ResultsData
	var err error

	fmt.Printf("Searching for: %s using %s engine in %s mode\n", query, searchType, searchMode)

	// Handle different search engines based on type parameter
	switch searchType {
	case "weaviate":
		templateResults, _ := weaviateSearch(query, searchMode)
		tmpl := template.New("results.html").Funcs(template.FuncMap{
            "hasPrefix": strings.HasPrefix,
        })
        tmpl = template.Must(tmpl.ParseFS(templates, "results.html"))
		templateVars := TemplateSearchResults{
			Results: templateResults.Results,
			Query:   query,
		}
		if err := tmpl.Execute(w, templateVars); err != nil {
			log.Printf("Error executing template: %v", err)
			http.Error(w, fmt.Sprintf("Template error: %v", err), http.StatusInternalServerError)
		}
	case "ball":
		searchResults, err = vectorSearch(query, 50)
	case "grid":
		if bigramGrid != nil && trigramGrid != nil {
			searchResults, err = gridSearch(query, 50)
		}
	case "kv":
		if kvStore != nil {
			searchResults, err = SearchKV(query, 50)
		}
	case "wcd":
		searchResults, err = wcdSearch(query, 50)
	default:
		// Default to ball tree search
		searchResults, err = vectorSearch(query, 50)
	}

	if err != nil {
		log.Printf("Search error: %v", err)
		http.Error(w, fmt.Sprintf("Search failed: %v", err), http.StatusInternalServerError)
		return
	}

	templateVars := TemplateSearchResults{
		Results: searchResults,
		Query:   query,
	}


	tmpl := template.New("results.html").Funcs(template.FuncMap{
        "hasPrefix": strings.HasPrefix,
    })
    tmpl = template.Must(tmpl.ParseFS(templates, "results.html"))
	if err := tmpl.Execute(w, templateVars); err != nil {
		log.Printf("Error executing template: %v", err)
		http.Error(w, fmt.Sprintf("Template error: %v", err), http.StatusInternalServerError)
	}
}

func rebuildIndexHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodPost {
		http.Error(w, "Method not allowed", http.StatusMethodNotAllowed)
		return
	}

	// Start rebuild in background
	go func() {
		if err := RebuildIndex(); err != nil {
			log.Printf("Error rebuilding index: %v", err)
		} else {
			log.Printf("Index rebuild completed successfully")
		}
	}()

	// Send properly formatted JSON response
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(map[string]string{"status": "rebuild started"})
}

func initSearchSystem() error {
	flag.BoolVar(&reindexAtStartup, "reindex", false, "Rebuild search index on startup")

	if reindexAtStartup {
		fmt.Println("Rebuilding search index...")
		if err := RebuildIndex(); err != nil {
			return fmt.Errorf("failed to rebuild index: %v", err)
		}
		return nil
	}

	// Open existing stores
	if err := OpenStores("./kv_index"); err != nil {
		return fmt.Errorf("failed to open stores: %v", err)
	}

	return nil
}

// OpenStores just opens existing stores without rebuilding
func OpenStores(path string) error {
	storeMutex.Lock()
	defer storeMutex.Unlock()

	store, err := ensemblekv.NewExtentKeyValueStore(filepath.Join(path, "search_index"), 8)
	if err != nil {
		return fmt.Errorf("failed to open KV store: %v", err)
	}

	vs, err := vectorstore.NewVectorStores(filepath.Join(path, "vectors"))
	if err != nil {
		store.Close()
		return fmt.Errorf("failed to open vector stores: %v", err)
	}

	kvStore = store
	vecStore = vs
	storePath = path

	return nil
}

func jobListHandler(w http.ResponseWriter, r *http.Request) {
	jobsMutex.Lock()
	jobList := make([]*Job, 0, len(jobs))
	for _, job := range jobs {
		jobList = append(jobList, job)
	}
	jobsMutex.Unlock()

	sort.Slice(jobList, func(i, j int) bool {
		return jobList[i].StartTime.After(jobList[j].StartTime)
	})

	tmpl := template.Must(template.New("job-list").Parse(`
		{{range .}}
		<div class="job">
			<h3>Job: {{.ID}}</h3>
			<p>Directory: {{.Directory}}</p>
			<p>Status: {{.Status}}</p>
			{{if eq .Status "Running"}}
				<div class="spinner"></div>
			{{end}}
			{{if eq .Status "Running"}}
				<button class="cancel-btn" hx-post="/cancel-job" hx-vals='{"jobID": "{{.ID}}"}'>Cancel</button>
			{{end}}
			<pre>{{.Output}}</pre>
		</div>
		{{else}}
		<p>No running jobs.</p>
		{{end}}
	`))

	tmpl.Execute(w, jobList)
}

func cancelJobHandler(w http.ResponseWriter, r *http.Request) {
	jobID := r.FormValue("jobID")

	jobsMutex.Lock()
	if job, exists := jobs[jobID]; exists {
		job.Status = "Cancelled"
		// TODO: Implement actual cancellation of the indexer process
	}
	jobsMutex.Unlock()

	jobListHandler(w, r)
}

// Sample data for autocomplete
var sampleWords = []string{"foo", "bar", "baz", "apple", "banana", "grape"}

// AutocompleteHandler returns a JSON array of strings based on the provided prefix
func AutocompleteHandler(w http.ResponseWriter, r *http.Request) {
	query := r.URL.Query()
	prefix := query.Get("prefix")

	matchingWords := []string{}

	result, err := runPrompt(`Complete the following piece of golang code:

	`+prefix, os.Getenv("OPENAI_API_KEY"))
	if err != nil {
		log.Printf("Error running prompt: %v", err)
		http.Error(w, "Error running prompt", http.StatusInternalServerError)
		return
	}

	matchingWords = append(matchingWords, result)

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(matchingWords)
}

func resultsHandler(w http.ResponseWriter, r *http.Request) {
	resultId := r.URL.Query().Get("id")
	if resultId == "" {
		log.Printf("No result id provided")
		http.Redirect(w, r, "/", http.StatusSeeOther)
		return
	}

	var jsonData []byte
	var err error

	// Try vector store first
	if kvStore != nil {
		data, err := kvStore.Get([]byte(resultId))
		if err == nil {
			var doc YoinkDocument
			if err := json.Unmarshal(data, &doc); err == nil {
				// Convert to pretty JSON
				jsonData, err = json.MarshalIndent(doc, "", "  ")
				if err == nil {
					w.Write(jsonData)
					return
				}
			}
		}
	}

	// Fallback to Weaviate if not found in vector store
	res := sweaviate.GetObject(resultId)
	jsonData, err = json.MarshalIndent(res, "", "  ")
	if err != nil {
		log.Printf("Error converting result to json: %v", err)
		http.Redirect(w, r, "/", http.StatusSeeOther)
		return
	}

	w.Write(jsonData)
}

func homeHandler(w http.ResponseWriter, r *http.Request) {
	tmpl := template.Must(template.ParseFS(templates, "index.html"))
	tmpl.Execute(w, nil)
}

// Update launchJobHandler to include yoink option
func launchJobHandler(w http.ResponseWriter, r *http.Request) {
	indexerType := r.FormValue("indexer")
	jobID := fmt.Sprintf("job-%d", time.Now().UnixNano())

	job := &Job{
		ID:        jobID,
		Type:      indexerType,
		Status:    "Running",
		StartTime: time.Now(),
	}

	switch indexerType {
	case "filesystem":
		job.Directory = r.FormValue("directory")
	case "website":
		job.URL = r.FormValue("url")
		job.Depth, _ = strconv.Atoi(r.FormValue("depth"))
		job.NumSpiders, _ = strconv.Atoi(r.FormValue("num-spiders"))
		job.MatchURL = r.FormValue("match-url")
	case "golang":
		job.Directory = r.FormValue("directory")
	case "yoink":
		job.Directory = r.FormValue("directory")
	default:
		http.Error(w, "Invalid indexer type", http.StatusBadRequest)
		return
	}

	jobsMutex.Lock()
	jobs[jobID] = job
	jobsMutex.Unlock()

	go func() {
		var err error
		if job.Type == "yoink" {
			err = runYoinkJob(job)
		} else {
			runIndexJob(job)
			return
		}

		if err != nil {
			jobsMutex.Lock()
			job.Status = "Error"
			job.Output = err.Error()
			jobsMutex.Unlock()
		}
	}()

	jobListHandler(w, r)
}

func SaveHandler(w http.ResponseWriter, r *http.Request) {
	// Save the data sent from the webpage editor to weaviate
	// The data is sent as a JSON object
	// The JSON object contains the following properties:
	// - id: the id of the document in weaviate
	// - content: the text to save

	//Read the body
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		log.Printf("Error reading body: %v", err)
		http.Error(w, "Error reading body", http.StatusInternalServerError)
		return
	}
	//Parse the body as JSON
	var data map[string]string
	err = json.Unmarshal(body, &data)
	if err != nil {
		log.Printf("Error parsing body as JSON: %v", err)
		http.Error(w, "Error parsing body as JSON", http.StatusInternalServerError)
		return
	}
	//Get the id and content
	id := data["id"]
	content := data["content"]
	fmt.Println("Saving document with id", id, "and content", content)
	//Load the original document from weaviate
	doc := sweaviate.GetObject(id)
	//Update the content
	doc.Properties["source"] = content

	//Save the document
	sweaviate.UpdateObject("Code", id, doc)

}

// Update the function and send it back
func AddDocumentationHandler(w http.ResponseWriter, r *http.Request) {
	// The data is sent as a JSON object
	// The JSON object contains the following properties:
	// - id: the id of the document in weaviate
	// - content: the text to save

	//Read the body
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		log.Printf("Error reading body: %v", err)
		http.Error(w, "Error reading body", http.StatusInternalServerError)
		return
	}
	//Parse the body as JSON
	var data map[string]string
	err = json.Unmarshal(body, &data)
	if err != nil {
		log.Printf("Error parsing body as JSON: %v", err)
		http.Error(w, "Error parsing body as JSON", http.StatusInternalServerError)
		return
	}
	//Get the id and content
	id := data["id"]
	content := data["content"]
	fmt.Println("Add documentation document with id", id, "and content", content)
	//Update the content

	updated, err := runPrompt(`Add documentation to this function.  Print only the updated function do not add commentary or apologies.

EXAMPLE
----------------
INPUT FUNCTION:
func hello() {
	fmt.Println("Hello, World!")
}

UPDATED FUNCTION:
// Print hello world
func hello() {
	fmt.Println("Hello, World!")
}
----------------

----------------
INPUT FUNCTION:
`+content+`
UPDATED FUNCTION:
`, os.Getenv("OPENAI_API_KEY"))

	type returnObject struct {
		ID      string `json:"id"`
		Content string `json:"content"`
	}

	ret := returnObject{
		ID:      id,
		Content: updated,
	}
	//Send the document back to the webpage
	json.NewEncoder(w).Encode(ret)
}

// Rebuild a package. This is not a hack, stop calling it a hack. No, you're a hack
func rebuildPackage(pkg string) {
	//Search weaviate for all the functions in the package
	functions := sweaviate.SearchByProperty("Code", "package", pkg)

	// Split the functions into files
	files := map[string][]map[string]string{}
	for _, function := range functions {
		filename := function["filename"]
		functions := files[filename]
		functions = append(functions, function)
		files[filename] = functions
	}

	//Sort the functions in the files by position
	for filename, functions := range files {

		sort.Slice(functions, func(i, j int) bool {
			a := functions[i]["position"]
			b := functions[j]["position"]
			a_num, _ := strconv.Atoi(a)
			b_num, _ := strconv.Atoi(b)
			return a_num < b_num
		})
		files[filename] = functions

	}

	filesource := map[string]string{}
	// For each file, concatenate the functions together
	for filename, functions := range files {
		var source string = filesource[filename]
		for _, function := range functions {
			source += function["source"]
		}

		filesource[filename] = source
	}

	// Write all the files out to disk
	for filename, source := range filesource {
		err := ioutil.WriteFile(filename, []byte(source), 0644)
		if err != nil {
			panic(err)
		}
	}
}

func RebuildHandler(w http.ResponseWriter, r *http.Request) {
	// Rebuild a package
	// The package is sent as a query parameter
	// The package is rebuilt by concatenating all the functions in the package
	// The functions are sorted by position before concatenation

	//Get the package name from the query
	pkg := r.URL.Query().Get("package")
	if pkg == "" {
		log.Printf("No package name provided")
		http.Redirect(w, r, "/", http.StatusSeeOther)
		return
	}
	//Rebuild the package
	rebuildPackage(pkg)
	//Redirect to the index page
	http.Redirect(w, r, "/", http.StatusSeeOther)
}

func browseHandler(w http.ResponseWriter, r *http.Request) {
	var requestData struct {
		Path string `json:"path"`
	}

	err := json.NewDecoder(r.Body).Decode(&requestData)
	if err != nil {
		http.Error(w, "Invalid request payload", http.StatusBadRequest)
		return
	}

	path := requestData.Path
	if path == "" {
		path = "."
	}

	var response DirectoryContent
	response.Path, err = filepath.Abs(path)
	if err != nil {
		response.Error = "Invalid path"
		json.NewEncoder(w).Encode(response)
		return
	}

	files, err := os.ReadDir(path)
	if err != nil {
		response.Error = "Path not found"
		json.NewEncoder(w).Encode(response)
		return
	}

	for _, file := range files {
		if file.IsDir() {
			response.Directories = append(response.Directories, file.Name())
		} else {
			response.Files = append(response.Files, file.Name())
		}
	}

	// Limit the number of files displayed to 20
	if len(response.Files) > 20 {
		response.Files = response.Files[:20]
	}

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(response)
}
