package main

import (
	"compress/gzip"
	"encoding/json"
	"fmt"
	"log"
	"os"
	"path/filepath"
	"sort"
	"strings"
	"sync"
	"time"

	"bufio"

	"gitlab.com/donomii/TagExplorer/vectorstore"
	"github.com/donomii/ensemblekv"
)

var (
    kvStore     ensemblekv.KvLike
    vecStore    *vectorstore.VectorStores
    storeMutex  sync.RWMutex
    storePath   string
)

func InitStores(path string) error {
    storeMutex.Lock()
    defer storeMutex.Unlock()

    if err := os.MkdirAll(path, 0755); err != nil {
        return fmt.Errorf("failed to create store directory: %v", err)
    }

    store, err := ensemblekv.NewExtentKeyValueStore(filepath.Join(path, "search_index"), 8)
    if err != nil {
        return fmt.Errorf("failed to create KV store: %v", err)
    }

    vs, err := vectorstore.NewVectorStores(filepath.Join(path, "vectors"))
    if err != nil {
        store.Close()
        return fmt.Errorf("failed to create vector stores: %v", err)
    }

    kvStore = store
    vecStore = vs
    storePath = path

    _,err = loadDocuments()
    return err
}

func loadDocuments() (map[string]bool, error) {
    log.Printf("Starting document loading process...")
    
    // Count total documents first
    var totalDocs int
    log.Printf("Counting total documents in store...")
    kvStore.MapFunc(func(key, value []byte) error {

            totalDocs++

        return nil
    })
    log.Printf("Found %d documents to process", totalDocs)

    var docsProcessed int
    var vectorsStored int
    lastProgress := time.Now()
    progressInterval := time.Second * 2

    _, err := kvStore.MapFunc(func(key, value []byte) error {


        docsProcessed++
        
        // Show progress every 2 seconds or every 1000 documents
        if time.Since(lastProgress) > progressInterval || docsProcessed%1000 == 0 {
            log.Printf("Progress: %d/%d documents (%.1f%%), %d vectors stored, current key: %s",
                docsProcessed, totalDocs, 
                float64(docsProcessed)/float64(totalDocs)*100,
                vectorsStored,
                string(key))
            lastProgress = time.Now()
        }

        var doc YoinkDocument
        if err := json.Unmarshal(value, &doc); err != nil {
            log.Printf("Error unmarshaling document %s: %v", string(key), err)
            return nil
        }

        // Store each vector type if present
        vectorTypes := []struct {
            name      string
            storeType string
            vec       []float64
        }{
            {"content bigrams", vectorstore.ContentBigrams, doc.ContentMod26Bigram},
            {"path bigrams", vectorstore.PathBigrams, doc.PathMod26Bigram},
            {"content trigrams", vectorstore.ContentTrigrams, doc.ContentMod26Trigram},
            {"path trigrams", vectorstore.PathTrigrams, doc.PathMod26Trigram},
        }

        for _, vt := range vectorTypes {
            if len(vt.vec) > 0 {
                if err := vecStore.StoreVector(vt.storeType, doc.GUID, vt.vec); err != nil {
                    log.Printf("Error storing %s for doc %s: %v", vt.name, doc.GUID, err)
                } else {
                    vectorsStored++
                    if vectorsStored%10000 == 0 {
                        log.Printf("Vector milestone: stored %d vectors", vectorsStored)
                    }
                }
            }
        }

        return nil
    })

    log.Printf("Document loading complete - Processed %d documents, stored %d vectors", 
        docsProcessed, vectorsStored)
    return nil, err
}

func SearchKV(query string, limit int) ([]ResultsData, error) {
    storeMutex.RLock()
    defer storeMutex.RUnlock()

    if kvStore == nil || vecStore == nil {
        return nil, fmt.Errorf("stores not initialized")
    }

    // split the query into words
    words := strings.Fields(query)
    if len(words) == 0 {
        return nil, fmt.Errorf("no query terms")
    }

    // Remove any words start with a '-' and store them as negative terms
    var negWords []string
    for i := 0; i < len(words); {
        if strings.HasPrefix(words[i], "-") {
            negWords = append(negWords, words[i][1:])
            words = append(words[:i], words[i+1:]...)
        } else {
            i++
        }
    }



    // Generate query vectors for positive terms
    queryBytes := []byte(strings.Join(words, " "))
    bigrams, err := vectorstore.Mod26Vector(queryBytes)
    if err != nil {
        return nil, err
    }


    trigrams, err := vectorstore.TrigramVector(queryBytes)
    if err != nil {
        return nil, err
    }

    // Generate query vectors for negative terms
    var negBigrams, negTrigrams []float64
    if len(negWords) > 0 {
        negBytes := []byte(strings.Join(negWords, " "))
        negBigrams, err = vectorstore.Mod26Vector(negBytes)
        if err != nil {
            return nil, err
        }
        negTrigrams, err = vectorstore.TrigramVector(negBytes)
        if err != nil {
            return nil, err
        }
    }


    // Search each vector type
    vectorTypes := []struct {
        storeType string
        vec       []float64
        negVec    []float64
    }{
        {vectorstore.ContentBigrams, bigrams, negBigrams},
        {vectorstore.PathBigrams, bigrams, negBigrams},
        {vectorstore.ContentTrigrams, trigrams, negTrigrams},
        {vectorstore.PathTrigrams, trigrams, negTrigrams},
    }

    // Collect results from all vector types
    docScores := make(map[string]float64)
    for _, vt := range vectorTypes {
        results, err := vecStore.Search(vt.storeType, vt.vec, vt.negVec, limit*2)
        if err != nil {
            log.Printf("Error searching %s: %v", vt.storeType, err)
            continue
        }
        for _, r := range results {
            docScores[r.DocID] += r.Score
        }
    }

    // Sort by combined scores
    type scoredDoc struct {
        docID string
        score float64
    }
    var sorted []scoredDoc
    for docID, score := range docScores {
        sorted = append(sorted, scoredDoc{docID, score})
    }
    sort.Slice(sorted, func(i, j int) bool {
        return sorted[i].score > sorted[j].score
    })

    if len(sorted) > limit {
        sorted = sorted[:limit]
    }

    // Convert to ResultsData
    results := make([]ResultsData, 0, len(sorted))
    for _, sd := range sorted {
        data, err := kvStore.Get([]byte(sd.docID))
        if err != nil {
            log.Printf("Error getting doc %s: %v", sd.docID, err)
            continue
        }

        var doc YoinkDocument
        if err := json.Unmarshal(data, &doc); err != nil {
            log.Printf("Error parsing document %s: %v", sd.docID, err)
            continue
        }

        results = append(results, ResultsData{
            Id:       doc.GUID,
            DocId:    doc.GUID,
            URL:      "file://" + doc.Source,
            Title:    filepath.Base(doc.Source),
            Text:     fmt.Sprintf("Score: %.2f - Type: %s", sd.score, doc.Type),
            Path:     doc.Source,
            FullText: fmt.Sprintf("%v", doc.Data["content"]),
            Class:    "Document",
        })
    }

    return results, nil
}

func CloseStores() error {
    storeMutex.Lock()
    defer storeMutex.Unlock()

    if kvStore != nil {
        if err := kvStore.Close(); err != nil {
            return err
        }
        kvStore = nil
    }

    if vecStore != nil {
        /* FIXME
        if err := vecStore.Close(); err != nil {
            return err
        }
            */
        vecStore = nil
    }

    return nil
}

// RebuildIndex performs a complete rebuild of all stores
func RebuildIndex() error {
    log.Printf("Starting complete index rebuild...")

    // Create temporary paths
    tempKVPath := filepath.Join(storePath, "search_index_temp")
    tempVecPath := filepath.Join(storePath, "vectors_temp")

    // Create new temporary stores
    newStore, err := ensemblekv.NewExtentKeyValueStore(tempKVPath, 8)
    if err != nil {
        return fmt.Errorf("failed to create new store: %v", err)
    }

    newVecStore, err := vectorstore.NewVectorStores(tempVecPath)
    if err != nil {
        newStore.Close()
        return fmt.Errorf("failed to create new vector stores: %v", err)
    }

    // Process all documents from index directory
    matches, err := filepath.Glob(filepath.Join(indexDirectory, "*.gz"))
    if err != nil {
        return fmt.Errorf("failed to list index files: %v", err)
    }

    log.Printf("Found %d index files to process", len(matches))

    var totalDocs, totalVectors uint32
    for fileNum, path := range matches {
        log.Printf("Processing index file %d/%d: %s", fileNum+1, len(matches), filepath.Base(path))
        docs, vecs, err := processIndexFile(path, newStore, newVecStore)
        if err != nil {
            log.Printf("Error processing %s: %v", path, err)
            continue
        }
        totalDocs += docs
        totalVectors += vecs
    }

    // Close existing stores
    if err := CloseStores(); err != nil {
        return fmt.Errorf("failed to close existing stores: %v", err)
    }

    // Swap in new stores
    if err := swapStores(tempKVPath, tempVecPath); err != nil {
        return fmt.Errorf("failed to swap stores: %v", err)
    }

    // Reinitialize memory structures
    if err := initBallTrees(); err != nil {
        log.Printf("Warning: Failed to initialize ball trees: %v", err)
    }
    if err := initGrids(); err != nil {
        log.Printf("Warning: Failed to initialize grids: %v", err)
    }

    log.Printf("Index rebuild complete: processed %d documents, %d vectors", 
        totalDocs, totalVectors)
    return nil
}

// swapStores replaces the old stores with new ones
func swapStores(newKVPath, newVecPath string) error {
    storeMutex.Lock()
    defer storeMutex.Unlock()

    oldKVPath := filepath.Join(storePath, "search_index")
    oldVecPath := filepath.Join(storePath, "vectors")
    
    // Move old stores to deletion directory
    deleteDir := filepath.Join(storePath, fmt.Sprintf("old_%d", time.Now().UnixNano()))
    if err := os.MkdirAll(deleteDir, 0755); err != nil {
        return fmt.Errorf("failed to create deletion directory: %v", err)
    }
    
    os.Rename(oldKVPath, filepath.Join(deleteDir, "search_index"))
    os.Rename(oldVecPath, filepath.Join(deleteDir, "vectors"))

    // Move new stores into place
    if err := os.Rename(newKVPath, oldKVPath); err != nil {
        return fmt.Errorf("failed to move new KV store: %v", err)
    }
    if err := os.Rename(newVecPath, oldVecPath); err != nil {
        return fmt.Errorf("failed to move new vector store: %v", err)
    }

    // Schedule deletion of old stores
    go func() {
        time.Sleep(time.Second)
        os.RemoveAll(deleteDir)
    }()

    return OpenStores(oldKVPath)
}


func processIndexFile(path string, store ensemblekv.KvLike, vecStore *vectorstore.VectorStores) (docs uint32, vectors uint32, err error) {
    file, err := os.Open(path)
    if err != nil {
        return 0, 0, fmt.Errorf("failed to open %s: %v", path, err)
    }
    defer file.Close()

    gzReader, err := gzip.NewReader(file)
    if err != nil {
        return 0, 0, fmt.Errorf("failed to create gzip reader: %v", err)
    }
    defer gzReader.Close()

    scanner := bufio.NewScanner(gzReader)
    scanner.Buffer(make([]byte, 1024*1024), 10*1024*1024) // 10MB buffer

    for scanner.Scan() {
        var doc YoinkDocument
        if err := json.Unmarshal(scanner.Bytes(), &doc); err != nil {
            log.Printf("Error unmarshaling document: %v", err)
            continue
        }

        // Store document in KV store
        if err := store.Put([]byte(doc.GUID), scanner.Bytes()); err != nil {
            log.Printf("Error storing document: %v", err)
            continue
        }

        // Store vectors
        vectorTypes := []struct {
            storeType string
            vec       []float64
        }{
            {vectorstore.ContentBigrams, doc.ContentMod26Bigram},
            {vectorstore.PathBigrams, doc.PathMod26Bigram},
            {vectorstore.ContentTrigrams, doc.ContentMod26Trigram},
            {vectorstore.PathTrigrams, doc.PathMod26Trigram},
        }

        for _, vt := range vectorTypes {
            if len(vt.vec) > 0 {
                if err := vecStore.StoreVector(vt.storeType, doc.GUID, vt.vec); err != nil {
                    log.Printf("Error storing %s for doc %s: %v", vt.storeType, doc.GUID, err)
                    continue
                }
                vectors++
            }
        }

        docs++
    }

    if err := scanner.Err(); err != nil {
        return docs, vectors, fmt.Errorf("error reading file: %v", err)
    }

    return docs, vectors, nil
}