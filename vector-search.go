package main

import (
	"bufio"
	"compress/gzip"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"os"
	"path/filepath"
	"sort"
	"strings"
)

// vectorSearch performs vector search across all gzipped index files

func vectorSearch(query string, limit int) ([]ResultsData, error) {
    treeMutex.RLock()
    defer treeMutex.RUnlock()

    if contentBigramTree == nil || contentTrigramTree == nil {
        return nil, fmt.Errorf("trees not initialized")
    }

    log.Printf("Starting vector search for query: %q", query)
    queryBytes := []byte(query)

    // Generate query vectors
    queryBigrams, err := Mod26Vector(queryBytes)
    if err != nil {
        return nil, err
    }

    queryTrigrams, err := trigramVector(queryBytes)
    if err != nil {
        return nil, err
    }

    // Create query points
    bigramQuery := BallTreePoint{Vector: queryBigrams}
    trigramQuery := BallTreePoint{Vector: queryTrigrams}

    // Get nearest neighbors from all trees
    seen := make(map[string]bool)
    scores := make(map[string]float64)

    // Helper function to process neighbors
    processNeighbors := func(neighbors []BallTreePoint, weight float64) {
        for _, n := range neighbors {
            if !seen[n.ID] {
                seen[n.ID] = true
                scores[n.ID] = 0
            }
            scores[n.ID] += weight * (1.0 / (1.0 + euclideanDistance(n.Vector, bigramQuery.Vector)))
        }
    }

    // Search all trees with appropriate weights
    processNeighbors(contentBigramTree.Query(bigramQuery, limit), 1.0)
    processNeighbors(pathBigramTree.Query(bigramQuery, limit), 0.5)
    processNeighbors(contentTrigramTree.Query(trigramQuery, limit), 1.5)
    processNeighbors(pathTrigramTree.Query(trigramQuery, limit), 0.75)

    // Convert results to sorted list
    type scoredDoc struct {
        id    string
        score float64
    }
    
    var sorted []scoredDoc
    for id, score := range scores {
        sorted = append(sorted, scoredDoc{id: id, score: score})
    }

    sort.Slice(sorted, func(i, j int) bool {
        return sorted[i].score > sorted[j].score
    })

    // Limit results and convert to ResultsData
    if len(sorted) > limit {
        sorted = sorted[:limit]
    }

    results := make([]ResultsData, 0, len(sorted))
    for _, sd := range sorted {
        data, err := kvStore.Get([]byte(sd.id))
        if err != nil {
            continue
        }

        var doc YoinkDocument
        if err := json.Unmarshal(data, &doc); err != nil {
            continue
        }

        results = append(results, ResultsData{
            Id:       doc.GUID,
            DocId:    doc.GUID,
            URL:      "file://" + doc.Source,
            Title:    filepath.Base(doc.Source),
            Text:     fmt.Sprintf("Score: %.2f - Type: %s", sd.score, doc.Type),
            Path:     doc.Source,
            FullText: fmt.Sprintf("%v", doc.Data["content"]),
            Class:    "Document",
        })
    }

    return results, nil
}

// searchSingleIndexWithNegative performs vector search on a single gzipped index file with negative term handling
func searchSingleIndexWithNegative(indexPath string, queryBigrams, queryTrigrams, negBigrams, negTrigrams []float64, resultsChan chan<- VectorSearchResult) {
    file, err := os.Open(indexPath)
    if err != nil {
        log.Printf("Failed to open index %s: %v", indexPath, err)
        return
    }
    defer file.Close()

    gzReader, err := gzip.NewReader(file)
    if err != nil {
        log.Printf("Failed to create gzip reader for %s: %v", indexPath, err)
        return
    }
    defer gzReader.Close()

    reader := bufio.NewReader(gzReader)

    // Read and process each document
    for {
        line, err := reader.ReadString('\n')
        if err == io.EOF {
            break
        }
        if err != nil {
			serrStr := err.Error()
			if strings.Contains(serrStr, "unexpected EOF") {
				log.Printf("Unexpected EOF error reading line from %s: %v", indexPath, err)
				break
			}

            log.Printf("Error reading line from %s: %v", indexPath, err)
            continue
        }

        var doc YoinkDocument
        if err := json.Unmarshal([]byte(line), &doc); err != nil {
            log.Printf("Error parsing JSON from %s: %v", indexPath, err)
            continue
        }

        // Calculate positive similarity score
        score := float64(0)
        if len(doc.ContentMod26Bigram) > 0 {
            score += cosineSimilarity(queryBigrams, doc.ContentMod26Bigram)
        }
        if len(doc.PathMod26Bigram) > 0 {
            score += cosineSimilarity(queryBigrams, doc.PathMod26Bigram)
        }
        if len(doc.PathMod26Trigram) > 0 {
            score += cosineSimilarity(queryTrigrams, doc.PathMod26Trigram)
        }
        if len(doc.ContentMod26Trigram) > 0 {
            score += cosineSimilarity(queryTrigrams, doc.ContentMod26Trigram)
        }

        // Calculate and subtract negative similarity score if negative terms exist
        if negBigrams != nil && negTrigrams != nil {
            negScore := float64(0)
            if len(doc.ContentMod26Bigram) > 0 {
                negScore += cosineSimilarity(negBigrams, doc.ContentMod26Bigram)
            }
            if len(doc.PathMod26Bigram) > 0 {
                negScore += cosineSimilarity(negBigrams, doc.PathMod26Bigram)
            }
            if len(doc.PathMod26Trigram) > 0 {
                negScore += cosineSimilarity(negTrigrams, doc.PathMod26Trigram)
            }
            if len(doc.ContentMod26Trigram) > 0 {
                negScore += cosineSimilarity(negTrigrams, doc.ContentMod26Trigram)
            }
            score -= negScore
        }

        resultsChan <- VectorSearchResult{
            Document: doc,
            Score:    score,
        }
    }
}
