package main

import (
    "math"
    "sort"
	"sync"
	"time"
	"encoding/json"
	"fmt"
    "sync/atomic"
	"log"
)

// Define ball trees for different vector types
var (
    contentBigramTree  *BallTree
    pathBigramTree    *BallTree
    contentTrigramTree *BallTree
    pathTrigramTree   *BallTree
    treeMutex         sync.RWMutex
)

// Helper function to convert float64 slices
func floatSliceToFloat64(slice []float64) []float64 {
    result := make([]float64, len(slice))
    copy(result, slice)
    return result
}

func initBallTrees() error {
    log.Printf("Starting ball tree initialization...")
    treeMutex.Lock()
    defer treeMutex.Unlock()

    contentBigramTree = NewBallTree(676, 40)    
    pathBigramTree = NewBallTree(676, 40)      
    contentTrigramTree = NewBallTree(17576, 40)
    pathTrigramTree = NewBallTree(17576, 40)   

    if kvStore == nil {
        return nil
    }

    var (
        docsProcessed int64
        lastProgress = time.Now()
        rateLimiter = time.NewTicker(100 * time.Millisecond)
        bigramPoints = make([]BallTreePoint, 0)
        pathBigramPoints = make([]BallTreePoint, 0)
        trigramPoints = make([]BallTreePoint, 0)
        pathTrigramPoints = make([]BallTreePoint, 0)
    )
    defer rateLimiter.Stop()

    _, err := kvStore.MapFunc(func(key, value []byte) error {
        <-rateLimiter.C
        atomic.AddInt64(&docsProcessed, 1)
        
        if time.Since(lastProgress) > time.Second*2 {
            log.Printf("Processing documents: %d docs so far...", atomic.LoadInt64(&docsProcessed))
            lastProgress = time.Now()
        }

        var doc YoinkDocument
        if err := json.Unmarshal(value, &doc); err != nil {
            return nil
        }

        if len(doc.ContentMod26Bigram) > 0 {
            bigramPoints = append(bigramPoints, BallTreePoint{Vector: doc.ContentMod26Bigram, ID: doc.GUID})
        }
        if len(doc.PathMod26Bigram) > 0 {
            pathBigramPoints = append(pathBigramPoints, BallTreePoint{Vector: doc.PathMod26Bigram, ID: doc.GUID})
        }
        if len(doc.ContentMod26Trigram) > 0 {
            trigramPoints = append(trigramPoints, BallTreePoint{Vector: doc.ContentMod26Trigram, ID: doc.GUID})
        }
        if len(doc.PathMod26Trigram) > 0 {
            pathTrigramPoints = append(pathTrigramPoints, BallTreePoint{Vector: doc.PathMod26Trigram, ID: doc.GUID})
        }
        return nil
    })

    if err != nil {
        return fmt.Errorf("error processing documents: %v", err)
    }

    totalPoints := len(bigramPoints) + len(pathBigramPoints) + len(trigramPoints) + len(pathTrigramPoints)
    log.Printf("Building trees with %d total points...", totalPoints)

    contentBigramTree.Build(bigramPoints)
    pathBigramTree.Build(pathBigramPoints) 
    contentTrigramTree.Build(trigramPoints)
    pathTrigramTree.Build(pathTrigramPoints)

    log.Printf("Ball tree initialization complete: processed %d documents, inserted %d points", 
        atomic.LoadInt64(&docsProcessed), totalPoints)

    return nil
}


// Point represents a point in n-dimensional space
type BallTreePoint struct {
    Vector []float64
    ID     string
}

// Node represents a node in the ball tree
type BallNode struct {
    Center     []float64
    Radius     float64
    Points     []BallTreePoint
    Left       *BallNode
    Right      *BallNode
    IsLeaf     bool
}

// BallTree is the main data structure
type BallTree struct {
    Root      *BallNode
    LeafSize  int  // Maximum points per leaf
    Dimension int  // Number of dimensions
}

// NewBallTree creates a new ball tree with given leaf size
func NewBallTree(dim, leafSize int) *BallTree {
    return &BallTree{
        LeafSize:  leafSize,
        Dimension: dim,
    }
}

// Build constructs the ball tree from a set of points
func (bt *BallTree) Build(points []BallTreePoint) {
    if len(points) == 0 {
        return
    }
    bt.Root = bt.buildNode(points)
}

// buildNode recursively builds a node of the ball tree
func (bt *BallTree) buildNode(points []BallTreePoint) *BallNode {
    node := &BallNode{
        Points: points,
    }
    
    // Calculate center and radius
    node.Center = calculateCenter(points)
    node.Radius = maxDistance(node.Center, points)
    
    // If we're at leaf size, stop here
    if len(points) <= bt.LeafSize {
        node.IsLeaf = true
        return node
    }
    
    // Find dimension with greatest spread
    dim := findSplitDimension(points)
    
    // Sort points by the chosen dimension
    sort.Slice(points, func(i, j int) bool {
        return points[i].Vector[dim] < points[j].Vector[dim]
    })
    
    // Split points into left and right groups
    mid := len(points) / 2
    node.Left = bt.buildNode(points[:mid])
    node.Right = bt.buildNode(points[mid:])
    
    return node
}

// Query finds k nearest neighbors to the given point
func (bt *BallTree) Query(query BallTreePoint, k int) []BallTreePoint {
    if bt.Root == nil {
        return nil
    }
    
    // Use a max heap to keep track of k nearest neighbors
    neighbors := newMaxHeap()
    bt.searchNode(bt.Root, query, k, neighbors, math.Inf(1))
    
    // Extract results in order
    result := make([]BallTreePoint, 0, k)
    for neighbors.Len() > 0 {
        result = append(result, neighbors.Pop().(heapItem).point)
    }
    
    // Reverse to get ascending distance order
    for i := 0; i < len(result)/2; i++ {
        result[i], result[len(result)-1-i] = result[len(result)-1-i], result[i]
    }
    
    return result
}

// searchNode recursively searches a node for nearest neighbors
func (bt *BallTree) searchNode(node *BallNode, query BallTreePoint, k int, neighbors *maxHeap, tau float64) float64 {
    if node == nil {
        return tau
    }
    
    // Calculate distance to node center
    dist := euclideanDistance(query.Vector, node.Center)
    
    // If this node is too far away, skip it
    if dist > tau + node.Radius {
        return tau
    }
    
    if node.IsLeaf {
        // Check all points in this leaf
        for _, p := range node.Points {
            d := euclideanDistance(query.Vector, p.Vector)
            if d < tau {
                neighbors.Push(heapItem{point: p, dist: d})
                if neighbors.Len() > k {
                    neighbors.Pop()
                }
                if neighbors.Len() == k {
                    tau = neighbors.Peek().(heapItem).dist
                }
            }
        }
    } else {
        // Recursively search children, visiting closest child first
        leftDist := euclideanDistance(query.Vector, node.Left.Center)
        rightDist := euclideanDistance(query.Vector, node.Right.Center)
        
        if leftDist < rightDist {
            tau = bt.searchNode(node.Left, query, k, neighbors, tau)
            tau = bt.searchNode(node.Right, query, k, neighbors, tau)
        } else {
            tau = bt.searchNode(node.Right, query, k, neighbors, tau)
            tau = bt.searchNode(node.Left, query, k, neighbors, tau)
        }
    }
    
    return tau
}

// Utility functions

func calculateCenter(points []BallTreePoint) []float64 {
    if len(points) == 0 {
        return nil
    }
    
    dim := len(points[0].Vector)
    center := make([]float64, dim)
    
    for _, p := range points {
        for i := 0; i < dim; i++ {
            center[i] += p.Vector[i]
        }
    }
    
    for i := 0; i < dim; i++ {
        center[i] /= float64(len(points))
    }
    
    return center
}

func maxDistance(center []float64, points []BallTreePoint) float64 {
    maxDist := 0.0
    for _, p := range points {
        dist := euclideanDistance(center, p.Vector)
        if dist > maxDist {
            maxDist = dist
        }
    }
    return maxDist
}

func findSplitDimension(points []BallTreePoint) int {
    if len(points) == 0 {
        return 0
    }
    
    dim := len(points[0].Vector)
    maxSpread := 0.0
    splitDim := 0
    
    for d := 0; d < dim; d++ {
        min := points[0].Vector[d]
        max := min
        
        for _, p := range points {
            if p.Vector[d] < min {
                min = p.Vector[d]
            }
            if p.Vector[d] > max {
                max = p.Vector[d]
            }
        }
        
        spread := max - min
        if spread > maxSpread {
            maxSpread = spread
            splitDim = d
        }
    }
    
    return splitDim
}

func euclideanDistance(a, b []float64) float64 {
    sum := 0.0
    for i := range a {
        diff := a[i] - b[i]
        sum += diff * diff
    }
    return math.Sqrt(sum)
}

// Priority queue implementation for k-nearest neighbors

type heapItem struct {
    point BallTreePoint
    dist  float64
}

type maxHeap []heapItem

func newMaxHeap() *maxHeap {
    h := &maxHeap{}
    return h
}

func (h maxHeap) Len() int { return len(h) }
func (h maxHeap) Less(i, j int) bool { return h[i].dist > h[j].dist } // Max heap
func (h maxHeap) Swap(i, j int) { h[i], h[j] = h[j], h[i] }

func (h *maxHeap) Push(x interface{}) {
    *h = append(*h, x.(heapItem))
}

func (h *maxHeap) Pop() interface{} {
    old := *h
    n := len(old)
    x := old[n-1]
    *h = old[0 : n-1]
    return x
}

func (h *maxHeap) Peek() interface{} {
    return (*h)[0]
}