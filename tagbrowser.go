package main

import (
	"bufio"
	"embed"
	"encoding/json"
	"flag"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"os"
	"os/exec"
	"path/filepath"
	"runtime/pprof"
	"sort"
	"strconv"
	"strings"
	"sync"
	"time"

	"bytes"

	"github.com/donomii/goof"
	"gitlab.com/donomii/weaviate-client"
)

type SearchResult struct {
	Title string `json:"title"`
	URL   string `json:"url"`
}

//go:embed index.html results.html task-manager.html
var templates embed.FS

//go:embed static/*
var staticFiles embed.FS

type SearchResults struct {
	Results []SearchResult `json:"results"`
}

// Global variables for Weaviate and HTTP server configuration
var (
	weaviateHostname string
	weaviatePort     string
	httpHostname     string
	httpPort         string
)

func init() {
	// Define command line flags for Weaviate and HTTP server configuration
	flag.StringVar(&weaviateHostname, "weaviate-hostname", "localhost", "Hostname of the Weaviate server")
	flag.StringVar(&weaviatePort, "weaviate-port", "8080", "Port of the Weaviate server")
	flag.StringVar(&httpHostname, "http-hostname", "", "Hostname of the HTTP server")
	flag.StringVar(&httpPort, "http-port", "8087", "Port of the HTTP server")
}

var ResultMapping = map[string]string{}

func addHeaders(fs http.Handler) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("X-Content-Type-Options", "asdfgasdfzszdfsdfsdf")
		fs.ServeHTTP(w, r)
	}
}

func startProfiling() {
	profileCount := 0
	cpuProfileCount := 0

	go func() {
		for {
			// Open CPU profile file
			cpuProfileCount++
			f, err := os.Create(fmt.Sprintf("cpu_%d.pprof", cpuProfileCount))
			if err != nil {
				log.Printf("Could not create CPU profile: %v", err)
				return
			}

			// Start CPU profiling
			if err := pprof.StartCPUProfile(f); err != nil {
				log.Printf("Could not start CPU profiling: %v", err)
				f.Close()
				return
			}

			// Ensure CPU profiling stops and file is closed on program exit

			// Wait for some time to collect profile data
			time.Sleep(30 * time.Second)
			pprof.StopCPUProfile()
			f.Close()
			log.Printf("CPU profile written to %s", f.Name())
		}
	}()

	// Start memory profiling routine
	go func() {
		for {
			time.Sleep(30 * time.Second)

			filename := fmt.Sprintf("memprofile_%d.pprof", profileCount)
			f, err := os.Create(filename)
			if err != nil {
				log.Printf("Could not create memory profile: %v", err)
				continue
			}

			if err := pprof.WriteHeapProfile(f); err != nil {
				log.Printf("Could not write memory profile: %v", err)
			}
			f.Close()
			log.Printf("Wrote memory profile to %s", filename)
			profileCount++
		}
	}()
}

func main() {
	var enableProfiling bool

	flag.BoolVar(&enableProfiling, "profile", false, "Enable CPU and memory profiling")

	// Parse command line flags
	flag.Parse()

	if enableProfiling {
		// Enable profiling
		startProfiling()
	}

	knownClasses := []string{"Person", "Messages", "Page", "Files", "Code", "Email", "Book"}
	for _, class := range knownClasses {
		fmt.Println("Creating class: " + class)
		weaviate.CreateClass(class, "text2vec-bigram")
	}
	ResultMapping["Messages:Text"] = "message" //
	ResultMapping["Messages:Title"] = "user"   //
	ResultMapping["Page:Text"] = "content"     //Class Code, property "source" is mapped to the template field Text
	ResultMapping["Page:Title"] = "title"      //Class Code, property "name" is mapped to the template field Title
	ResultMapping["Page:Path"] = "url"         //Class Code, property "url" is mapped to the template field Path
	ResultMapping["Book:Title"] = "path"
	ResultMapping["Book:Path"] = "url"
	ResultMapping["Book:Text"] = "text"
	ResultMapping["Book:URL"] = "path"
	ResultMapping["Page:URL"] = "url"         //Class Code, property "url" is mapped to the template field URL
	ResultMapping["Files:Text"] = "text"      //Class Files, property "text" is mapped to the template field Text
	ResultMapping["Files:Title"] = "path"     //Class Files, property "path" is mapped to the template field Title
	ResultMapping["Files:Path"] = "url"       //Class Files, property "url" is mapped to the template field Path
	ResultMapping["Files:URL"] = "url"        //Class Files, property "url" is mapped to the template field URL
	ResultMapping["Code:Package"] = "package" //Class Code, property "package" is mapped to the template field Package
	ResultMapping["Code:Title"] = "name"      //Class Code, property "name" is mapped to the template field Title
	ResultMapping["Code:Text"] = "source"     //Class Code, property "source" is mapped to the template field Text

	ResultMapping["Email:Text"] = "body"  //Class Email, property "body" is mapped to the template field Text
	ResultMapping["Email:Title"] = "from" //Class Email, property "title" is mapped to the template field Title
	//ResultMapping["Email:Path"] = "messageid"        //Class Email, property "messageid" is mapped to the template field Path

	//Make a map of mappings that convert the search results into ResultsData.  The key is the class name and TemplateName, joined with a colon like this: "Class:TemplateName".  The value is the property in weaviate that should be displayed here.

	/*
	   The currently available template names are:


	   	Id       string
	   	DocId    string
	   	URL      string
	   	Title    string
	   	Text     string
	   	Path     string
	   	FullText string
	   	Class    string
	   	Package  string

	*/

	//The command line arguments are the triplets of class, property, and template field name.  e.g. "Person:name:Name", "Person:website:Url", "Person:description:Text"

	//Command line arguments override the default mappings above.

	for _, arg := range flag.Args() {
		parts := strings.Split(arg, ":")
		if len(parts) != 3 {
			log.Printf("Invalid argument: %v", arg)
			os.Exit(1)
		}
		class := parts[0]
		property := parts[1]
		field := parts[2]
		ResultMapping[class+":"+field] = property
	}

	//Print all result mappings
	fmt.Printf("Result mappings: %+v\n", ResultMapping)

	weaviate.WeaviateUrl = "http://" + weaviateHostname + ":" + weaviatePort + "/v1/"

	http.HandleFunc("/autocomplete", AutocompleteHandler)
	http.HandleFunc("/search", searchHandler)
	http.HandleFunc("/save", SaveHandler)
	http.HandleFunc("/addDocumentation", AddDocumentationHandler)
	http.HandleFunc("/rebuild", RebuildHandler)
	http.HandleFunc("/results", resultsHandler)
	http.HandleFunc("/download", downloadHander)
	http.HandleFunc("/display", displayHandler)
	http.HandleFunc("/downloadobject", downloadObjectHander)
	http.HandleFunc("/task-manager", taskManagerHandler)
	http.HandleFunc("/launch-job", launchJobHandler)
	http.HandleFunc("/job-list", jobListHandler)
	http.HandleFunc("/cancel-job", cancelJobHandler)
	http.HandleFunc("/browse", browseHandler)
	// Initialize search system
	if err := initSearchSystem(); err != nil {
		log.Printf("Warning: Search system initialization failed: %v", err)
	}
	defer CloseStores()

	staticFileHandler := http.FileServer(http.FS(staticFiles))
	http.Handle("/static/", http.StripPrefix("/static", addHeaders(staticFileHandler)))

	wholeDisk := http.FileServer(http.Dir("/"))
	http.Handle("/files/", http.StripPrefix("/files/", addHeaders(wholeDisk)))

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		if r.URL.Path != "/" {
			staticFileHandler.ServeHTTP(w, r)
			return
		}
		homeHandler(w, r)
	})

	fmt.Println("Connecting to Weaviate at " + weaviate.WeaviateUrl)
	fmt.Println("Listening for user connections on " + httpHostname + ":" + httpPort + "...")
	http.ListenAndServe(httpHostname+":"+httpPort, nil)
}

type Job struct {
	ID         string    `json:"id"`
	Type       string    `json:"type"`
	Directory  string    `json:"directory"`
	URL        string    `json:"url"`
	Depth      int       `json:"depth"`
	NumSpiders int       `json:"numSpiders"`
	MatchURL   string    `json:"matchURL"`
	Status     string    `json:"status"`
	StartTime  time.Time `json:"startTime"`
	Output     string    `json:"output"`
}

var (
	jobs      = make(map[string]*Job)
	jobsMutex sync.Mutex
)

func runIndexJob(job *Job) {
	binPath := goof.ExecutablePath()
	var cmd *exec.Cmd

	switch job.Type {
	case "filesystem":
		cmd = exec.Command(binPath+"/indexer.exe", job.Directory)
	case "website":
		args := []string{  "-depth", strconv.Itoa(job.Depth), "-num-spiders", strconv.Itoa(job.NumSpiders), job.URL}
		if job.MatchURL != "" {
			args = append(args, "-match-url", job.MatchURL)
		}
		cmd = exec.Command(binPath+"/ripper.exe", args...)
	case "golang":
		cmd = exec.Command(binPath+"/goloader.exe", job.Directory)
	default:
		job.Status = "Error"
		job.Output = "Invalid indexer type"
		return
	}

	stdout, err := cmd.StdoutPipe()
	if err != nil {
		log.Printf("Error creating stdout pipe: %v", err)
		job.Status = "Error"
		job.Output = fmt.Sprintf("Error creating stdout pipe: %v", err)
		return
	}

	stderr, err := cmd.StderrPipe()
	if err != nil {
		log.Printf("Error creating stderr pipe: %v", err)
		job.Status = "Error"
		job.Output = fmt.Sprintf("Error creating stderr pipe: %v", err)
		return
	}

	fmt.Printf("Starting indexer with command: %v\n", cmd.String())

	if err := cmd.Start(); err != nil {
		log.Printf("Error starting indexer: %v", err)
		job.Status = "Error"
		job.Output = fmt.Sprintf("Error starting indexer: %v", err)
		return
	}

	go func() {
		scanner := bufio.NewScanner(io.MultiReader(stdout, stderr))
		for scanner.Scan() {
			jobsMutex.Lock()
			job.Output += scanner.Text() + "\n"
			if len(job.Output) > 1000 {
				job.Output = job.Output[len(job.Output)-1000:]
			}
			jobsMutex.Unlock()
		}
	}()

	if err := cmd.Wait(); err != nil {
		log.Printf("Indexer exited with error: %v", err)
		job.Status = "Error"
		job.Output += fmt.Sprintf("Indexer exited with error: %v", err)
	} else {
		job.Status = "Completed"
		job.Output += "Indexer completed successfully."
	}
}

// The results are a list of maps, each map is a result.  Merge the results based on score, which is in the map as "score".
// The return list must be sorted by score, highest first
func mergeResults(results [][]map[string]string) []map[string]string {
	var mergedResults []map[string]string
	for _, result := range results {
		for _, resultItem := range result {
			mergedResults = append(mergedResults, resultItem)
		}
	}
	//Sort the results by score
	sort.Slice(mergedResults, func(i, j int) bool {
		//Convert the scores from string to float
		scoreText1 := mergedResults[i]["Score"]
		score1, err := strconv.ParseFloat(scoreText1, 64)
		if err != nil {
			log.Printf("Error converting score*%v) to float: %v", scoreText1, err)
			return false
		}
		scoreText2 := mergedResults[j]["Score"]
		score2, err := strconv.ParseFloat(scoreText2, 64)

		if err != nil {
			log.Printf("Error converting score(%v) to float: %v", scoreText2, err)
			return false
		}
		return score1 > score2
	})
	return mergedResults
}

// Loads a file from disk and sends it
func downloadHander(w http.ResponseWriter, r *http.Request) {
	// download a file
	query := r.URL.Query().Get("id")
	if query == "" {
		log.Printf("No doc id provided")
		http.Redirect(w, r, "/", http.StatusSeeOther)
		return
	}

	obj := weaviate.GetObject(query)
	res := convertObject(obj)

	path := res.Path

	fmt.Printf("Download: sending path: %v\n", path)
	path = strings.TrimPrefix(path, "file://")
	//Load the file
	data, err := os.ReadFile(path)
	if err != nil {
		log.Printf("Error loading file %v: %v", path, err)
		http.Redirect(w, r, "/", http.StatusSeeOther)
		return
	}
	//Set the filename based on the path
	filename := filepath.Base(path)
	w.Header().Set("Content-Disposition", "attachment; filename="+filename)
	//Send the file
	w.Write(data)

}

type ResultsData struct {
	Id       string
	DocId    string
	URL      string
	Title    string
	Text     string
	Path     string
	FullText string
	Class    string
	Package  string
}

type TemplateSearchResults struct {
	Results []ResultsData
	Query   string
}

func ShortenToEnd(n int, s string) string {
	if len(s) > n {
		return s[n:]
	}
	return s
}

func disableHTML(s string) string {
	//Remove all > / & < characters
	s = strings.ReplaceAll(s, ">", "")
	s = strings.ReplaceAll(s, "<", "")
	s = strings.ReplaceAll(s, "&", "")
	return s
}

func convertResults(results []map[string]string, shortenStrings int) TemplateSearchResults {
	var templateResults []ResultsData
	fmt.Printf("ResultsMap: %v\n", ResultMapping)
	//Count the number of occurrences of each property
	var propCounts = map[string]int{}

	for _, result := range results {
		for prop, _ := range result {
			propCounts[prop]++
		}
		class := result["Class"]
		pathProperty := ResultMapping[class+":Path"]
		//idProperty := ResultMapping[class+":Id"]
		urlProperty := ResultMapping[class+":URL"]
		titleProperty := ResultMapping[class+":Title"]
		textProperty := ResultMapping[class+":Text"]
		packageProperty := ResultMapping[class+":Package"]

		titleProperty = sanitise(titleProperty)
		textProperty = sanitise(textProperty)
		pathProperty = sanitise(pathProperty)
		urlProperty = sanitise(urlProperty)
		url := result[urlProperty]
		if !strings.Contains(url, "://") {
			url = "/files/" + url
		}
		packageProperty = sanitise(packageProperty)

		//fmt.Printf("Text property: %v -> %v\n", class+":Text", textProperty )
		if shortenStrings > 0 {
			templateResults = append(templateResults, ResultsData{
				//URL:   template.URL(result[urlProperty]),
				Path:     sanitise(result[pathProperty]),
				Id:       sanitise(result["DocId"]),
				DocId:    sanitise(result["DocId"]),
				URL:      url,
				Title:    ShortenToEnd(shortenStrings, sanitise(result[titleProperty])),
				Text:     disableHTML(goof.ShortenString(shortenStrings, sanitise(result[textProperty]))),
				FullText: sanitise(goof.ShortenString(1000000, result[textProperty])), //FIXME don't dump all the text, but find a better limit
				Class:    class,
				Package:  sanitise(result[packageProperty]),
			})

		} else {
			templateResults = append(templateResults, ResultsData{
				Id:       sanitise(result["DocId"]),
				DocId:    sanitise(result["DocId"]),
				Path:     sanitise(result[pathProperty]),
				URL:      url,
				Title:    sanitise(result[titleProperty]),
				Text:     sanitise(result[textProperty]),
				FullText: sanitise(result[textProperty]),
				Class:    class,
				Package:  sanitise(result[packageProperty]),
			})
		}
	}
	fmt.Printf("Found these properties in the results: %v\n", propCounts)
	return TemplateSearchResults{Results: templateResults}
}

func sanitise(textProperty string) string {
	//Delete everything up to the first incidence of the word "title"
	/*_, after, found := strings.Cut(textProperty, "<body")
	if found {
		textProperty = after
	}
	_, after, found = strings.Cut(textProperty, "<BODY")
	if found {
		textProperty = after
	}
	textProperty = strings.ReplaceAll(textProperty, "<", " ")
	textProperty = strings.ReplaceAll(textProperty, ">", " ")
	textProperty = strings.ReplaceAll(textProperty, "\"", " ")
	textProperty = strings.ReplaceAll(textProperty, "'", " ")
	textProperty = strings.ReplaceAll(textProperty, "\\", " ")
	*/
	return textProperty
}

func convertObject(obj weaviate.GetObjectResult) ResultsData {
	//Convert the object to a map
	//obj.Properties
	result := map[string]string{}
	for key, value := range obj.Properties {
		result[key] = sanitise(value.(string))
	}

	class := sanitise(obj.Class)
	pathProperty := sanitise(ResultMapping[class+":Path"])
	//idProperty := sanitise(ResultMapping[class+":Id"])
	urlProperty := sanitise(ResultMapping[class+":URL"])
	titleProperty := sanitise(ResultMapping[class+":Title"])
	textProperty := sanitise(ResultMapping[class+":Text"])
	packageProperty := sanitise(ResultMapping[class+":Package"])

	res := ResultsData{
		Id:       sanitise(result["DocId"]),
		DocId:    sanitise(result["DocId"]),
		Path:     sanitise(result[pathProperty]),
		URL:      sanitise(result[urlProperty]),
		Title:    sanitise(result[titleProperty]),
		Text:     sanitise(result[textProperty]),
		FullText: sanitise(result[textProperty]),
		Class:    class,
		Package:  sanitise(result[packageProperty]),
	}

	return res

}

func search(query string) (*SearchResults, error) {
	u := url.URL{
		Scheme: "http",
		Host:   "localhost:8080",
		Path:   "/v1/search",
		RawQuery: url.Values{
			"query": []string{query},
		}.Encode(),
	}

	resp, err := http.Get(u.String())
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	var searchResults SearchResults
	err = json.NewDecoder(resp.Body).Decode(&searchResults)
	if err != nil {
		return nil, err
	}

	return &searchResults, nil
}

type Message struct {
	Role    string `json:"role"`
	Content string `json:"content"`
}

type Request struct {
	Model string `json:"model"`
	//Prompt string `json:"prompt"`
	Messages    []Message `json:"messages"`
	Temperature float64   `json:"temperature"`
	MaxTokens   int       `json:"max_tokens"`
	//TopP             float64  `json:"top_p"`
	//FrequencyPenalty float64  `json:"frequency_penalty"`
	//PresencePenalty  float64  `json:"presence_penalty"`
	//Stop []string `json:"stop"`
	//Stop string `json:"stop"`
}

type OpenAIResponse struct {
	ID string `json:"id"`

	Object  string   `json:"object"`
	Created int      `json:"created"`
	Model   string   `json:"model"`
	Choices []Choice `json:"choices"`
	Use     Usage    `json:"usage"`
}

type Choice struct {
	Text         string      `json:"text"`
	Index        int         `json:"index"`
	Message      Message     `json:"message"`
	Logprobs     interface{} `json:"logprobs"`
	FinishReason string      `json:"finish_reason"`
}

type Usage struct {
	PromptTokens     int `json:"prompt_tokens"`
	CompletionTokens int `json:"completion_tokens"`
	TotalTokens      int `json:"total_tokens"`
}

const openAIEndpoint = "https://api.openai.com/v1/chat/completions"

func runPrompt(request, apiKey string) (string, error) {

	ai_request := Request{
		Model: "gpt-3.5-turbo-16k",
		//Prompt: request,
		Messages:    []Message{{"user", request}},
		Temperature: 1.1,
		MaxTokens:   9128,
		//TopP:             1.0,
		//FrequencyPenalty: 0.0,
		//PresencePenalty:  0.0,
		//Stop: "\n",
	}

	jsonData, _ := json.Marshal(ai_request)

	fmt.Println("jsonData: ", string(jsonData))
	client := &http.Client{}
	req, _ := http.NewRequest("POST", openAIEndpoint, bytes.NewReader(jsonData))
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Authorization", "Bearer "+apiKey)

	log.Printf("Request: %+v\n", req)
	resp, err := client.Do(req)
	if err != nil {
		return "", err
	}
	defer resp.Body.Close()

	//Print response code
	log.Println("Response Status:", resp.Status)

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return "", err
	}
	log.Printf("ai response body: %+v\n", string(body))
	var aiResponse OpenAIResponse
	err = json.Unmarshal(body, &aiResponse)
	if err != nil {
		return "", err
	}

	log.Printf("response: %+v\n", aiResponse)
	if len(aiResponse.Choices) == 0 {
		return "", nil
	}
	response2 := aiResponse.Choices[0].Text
	response1 := ""
	if len(aiResponse.Choices[0].Text) == 0 {
		response1 = aiResponse.Choices[0].Message.Content
	}

	return response1 + response2, nil
}

var debug = false

type AIRequest struct {
	Model            string   `json:"model"`
	Prompt           string   `json:"prompt"`
	Temperature      float64  `json:"temperature"`
	MaxTokens        int      `json:"max_tokens"`
	TopP             float64  `json:"top_p"`
	FrequencyPenalty float64  `json:"frequency_penalty"`
	PresencePenalty  float64  `json:"presence_penalty"`
	Stop             []string `json:"stop"`
}

func makeOpenAIEndpoint(url string) string {
	return url + "completions"
}

// HTMX directory picker

type DirectoryContent struct {
	Path        string   `json:"path"`
	Directories []string `json:"directories"`
	Files       []string `json:"files"`
	Error       string   `json:"error,omitempty"`
}
