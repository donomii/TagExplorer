package main

import (
	"bufio"
	"bytes"
	"compress/gzip"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"os"
	"os/exec"
	"path/filepath"
	"time"

	"github.com/donomii/goof"
)

// runYoinkJob starts the yoinker process and saves output to a new gzipped index file
func runYoinkJob(job *Job) error {
	// Ensure index directory exists
	if err := os.MkdirAll(indexDirectory, 0755); err != nil {
		return fmt.Errorf("failed to create index directory: %v", err)
	}

	// Create new gzipped index file with timestamp
	timestamp := time.Now().Format("20060102-150405")
	indexPath := filepath.Join(indexDirectory, fmt.Sprintf("index-%s.gz", timestamp))
	
	indexFile, err := os.Create(indexPath)
	if err != nil {
		return fmt.Errorf("failed to create index file: %v", err)
	}
	defer indexFile.Close()

	gzWriter := gzip.NewWriter(indexFile)
	defer gzWriter.Close()

	binPath := goof.ExecutablePath()
	yoinkerPath := filepath.Join(binPath, "yoinker")

	cmd := exec.Command(yoinkerPath, job.Directory)
	stdout, err := cmd.StdoutPipe()
	if err != nil {
		return fmt.Errorf("failed to create stdout pipe: %v", err)
	}

	if err := cmd.Start(); err != nil {
		return fmt.Errorf("failed to start yoinker: %v", err)
	}

	reader := bufio.NewReaderSize(stdout, 1024*1024) // 1MB buffer

	for {
		line, err := reader.ReadBytes('\n')
		if err == io.EOF {
			break
		}
		if err != nil {
			log.Printf("Error reading line: %v", err)
			continue
		}

		// Skip empty lines
		if len(bytes.TrimSpace(line)) == 0 {
			continue
		}

		// Validate JSON
		var doc YoinkDocument
		if err := json.Unmarshal(line, &doc); err != nil {
			log.Printf("Invalid JSON document: %v", err)
			continue
		}

		// Write the line to gzipped file
		if _, err := gzWriter.Write(line); err != nil {
			log.Printf("Error writing to gzipped index: %v", err)
			continue
		}

		// Update job status
		jobsMutex.Lock()
		job.Output = fmt.Sprintf("Processed document: %s\n%s", doc.Source, job.Output)
		if len(job.Output) > 1000 {
			job.Output = job.Output[:1000]
		}
		jobsMutex.Unlock()
	}

	if err := cmd.Wait(); err != nil {
		return fmt.Errorf("yoinker process error: %v", err)
	}

	return nil
}