package main

import (
	"fmt"
	"math"
	"strings"
	"strconv"
	"container/heap"
	"log"
)

// Point represents a multi-dimensional point in the grid.
type Point struct {
	Coords []float64
	ID     string // Optional: For identifying points
}

// Bucket represents a container for points at a specific grid level.
type Bucket struct {
	Points []Point
	Bounds [][2]float64 // Bounds for each dimension (min, max)
}

// SparseGrid represents the overall grid structure.
type SparseGrid struct {
	Buckets map[string]*Bucket // Sparse storage for non-empty buckets
	Dims    int                // Number of dimensions
	CellSize float64           // Grid cell size for bucketing
}

// NewSparseGrid initializes a new SparseGrid.
func NewSparseGrid(dims int, cellSize float64) *SparseGrid {
	return &SparseGrid{
		Buckets: make(map[string]*Bucket),
		Dims:    dims,
		CellSize: cellSize,
	}
}

// getBucketKey generates a unique key for a bucket based on the coordinates.
func (grid *SparseGrid) getBucketKey(coords []float64) string {
	key := ""
	for _, c := range coords {
		key += fmt.Sprintf("%d:", int(math.Floor(c/grid.CellSize)))
	}
	return key
}

// getBucketBounds computes the bounds of a bucket based on its key.
func (grid *SparseGrid) getBucketBounds(key string) [][2]float64 {
	bounds := make([][2]float64, grid.Dims)
	cells := strings.Split(key, ":")
	for i := 0; i < grid.Dims; i++ {
		if cells[i] == "" {
			continue
		}
		cellIndex, _ := strconv.Atoi(cells[i])
		min := float64(cellIndex) * grid.CellSize
		max := min + grid.CellSize
		bounds[i] = [2]float64{min, max}
	}
	return bounds
}

// Insert adds a point into the grid.
func (grid *SparseGrid) Insert(point Point) {
	if len(point.Coords) != grid.Dims {
		panic("Point dimensions do not match grid dimensions")
	}
	key := grid.getBucketKey(point.Coords)
	if _, exists := grid.Buckets[key]; !exists {
		grid.Buckets[key] = &Bucket{
			Points: []Point{},
			Bounds: grid.getBucketBounds(key),
		}
	}
	grid.Buckets[key].Points = append(grid.Buckets[key].Points, point)
}

// generateOffsetChan returns a channel that yields the next valid grid cell within radius
func generateOffsetChan(dims int, radius float64) chan []int {
    ch := make(chan []int)
    
    go func() {
        defer close(ch)
        
        // Start at origin
        current := make([]int, dims)
        maxCoord := int(math.Ceil(radius))
        
        // Initialize to smallest coordinates
        for i := range current {
            current[i] = -maxCoord
        }
        
        // Keep generating until we've gone through all coordinates within bounds
        for {
            // Check if current point is within radius
            sumSquares := 0
            for _, v := range current {
                sumSquares += v * v
            }
            if math.Sqrt(float64(sumSquares)) <= radius {
                coords := make([]int, dims)
                copy(coords, current)
                ch <- coords
            }
            
            // Increment coordinates
            for d := 0; ; d++ {
                if d >= dims {
                    return // Done when we overflow the last dimension
                }
                current[d]++
                if current[d] <= maxCoord {
                    break
                }
                current[d] = -maxCoord // Reset this dimension and continue to next
            }
        }
    }()
    
    return ch
}

// GetNeighbors retrieves up to limit closest points within radius
// mapNeighbors applies a function to all valid neighbor offsets within radius
func mapNeighbors(dims int, radius float64, f func([]int)) {
    maxCoord := int(math.Ceil(radius))
    coords := make([]int, dims)
    
    var mapHelper func(depth int)
    mapHelper = func(depth int) {
        if depth == dims {
            distSq := 0
            for _, v := range coords {
                distSq += v * v
            }
            if math.Sqrt(float64(distSq)) <= radius {
                coordsCopy := make([]int, dims)
                copy(coordsCopy, coords)
                f(coordsCopy)
            }
            return
        }
        
        for i := -maxCoord; i <= maxCoord; i++ {
            coords[depth] = i
            mapHelper(depth + 1)
        }
    }
    
    mapHelper(0)
}

// GetNeighbors finds nearest neighbors within radius
func (grid *SparseGrid) GetNeighbors(query Point, radius float64, limit int) []Point {
    if len(query.Coords) != grid.Dims {
        panic("Query dimensions do not match grid dimensions")
    }

    pq := make(PriorityQueue, 0, limit)
    heap.Init(&pq)
    seen := make(map[string]bool)
    searchRange := radius / grid.CellSize
    baseKey := grid.getBucketKey(query.Coords)

    mapNeighbors(grid.Dims, searchRange, func(offset []int) {
        neighborKey := offsetBucketKey(baseKey, offset)
		log.Printf("Neighbor key: %s", neighborKey)
        if bucket, exists := grid.Buckets[neighborKey]; exists {
            for _, p := range bucket.Points {
                if !seen[p.ID] {
                    dist := distance(query.Coords, p.Coords)
                    if dist <= radius {
                        seen[p.ID] = true
                        item := &Item{
                            point:    p,
                            distance: dist,
                        }
                        if pq.Len() < limit {
                            heap.Push(&pq, item)
                        } else if dist < pq[0].distance {
                            heap.Pop(&pq)
                            heap.Push(&pq, item)
                        }
                    }
                }
            }
        }
    })

    result := make([]Point, pq.Len())
    for i := len(result) - 1; i >= 0; i-- {
        result[i] = heap.Pop(&pq).(*Item).point
    }
    return result
}

// Priority queue implementation for keeping top N points
type Item struct {
    point    Point
    distance float64
    index    int
}

type PriorityQueue []*Item

func (pq PriorityQueue) Len() int { return len(pq) }

func (pq PriorityQueue) Less(i, j int) bool {
    // We want a max-heap, so use greater than
    return pq[i].distance > pq[j].distance
}

func (pq PriorityQueue) Swap(i, j int) {
    pq[i], pq[j] = pq[j], pq[i]
    pq[i].index = i
    pq[j].index = j
}

func (pq *PriorityQueue) Push(x interface{}) {
    n := len(*pq)
    item := x.(*Item)
    item.index = n
    *pq = append(*pq, item)
}

func (pq *PriorityQueue) Pop() interface{} {
    old := *pq
    n := len(old)
    item := old[n-1]
    old[n-1] = nil
    item.index = -1
    *pq = old[0 : n-1]
    return item
}

// distance calculates Euclidean distance between two points.
func distance(a, b []float64) float64 {
	if len(a) != len(b) {
		panic("Point dimensions do not match")
	}
	sum := 0.0
	for i := 0; i < len(a); i++ {
		diff := a[i] - b[i]
		sum += diff * diff
	}
	return math.Sqrt(sum)
}

// generateOffsets generates all offsets within a given range in each dimension.
func generateOffsets(dims, rangeLimit int) [][]int {
	var result [][]int
	var helper func([]int, int)

	helper = func(current []int, depth int) {
		if depth == dims {
			newcopy := make([]int, dims)
			copy(current, newcopy)
			result = append(result, newcopy)
			return
		}
		for i := -rangeLimit; i <= rangeLimit; i++ {
			helper(append(current, i), depth+1)
		}
	}
	helper([]int{}, 0)
	return result
}

// offsetBucketKey applies an offset to a bucket key.
func offsetBucketKey(key string, offset []int) string {
	cells := strings.Split(key, ":")
	if len(cells) != len(offset)+1 {
		panic("Invalid offset or bucket key")
	}
	newKey := ""
	for i := 0; i < len(offset); i++ {
		cellIndex, _ := strconv.Atoi(cells[i])
		newKey += fmt.Sprintf("%d:", cellIndex+offset[i])
	}
	return newKey
}

func TestGrid() {

	maxPoints:=100
	// Create a 3-dimensional sparse grid with a cell size of 1.0
	grid := NewSparseGrid(3, 1.0)

	// Insert points
	grid.Insert(Point{Coords: []float64{1.1, 2.2, 3.3}, ID: "A"})
	grid.Insert(Point{Coords: []float64{1.4, 2.1, 3.0}, ID: "B"})
	grid.Insert(Point{Coords: []float64{4.0, 5.0, 6.0}, ID: "C"})

	// Query neighbors
	query := Point{Coords: []float64{1.2, 2.2, 3.1}}
	neighbors := grid.GetNeighbors(query, 1.5, maxPoints)

	fmt.Println("Neighbors:", neighbors)
}
