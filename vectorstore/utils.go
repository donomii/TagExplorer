package vectorstore

import (
    "math"
)

// cosineSimilarity calculates similarity between two vectors
func cosineSimilarity(vec1, vec2 []float64) float64 {
    var dotProduct float64
    var norm1, norm2 float64

    for i := 0; i < len(vec1); i++ {
        dotProduct += vec1[i] * vec2[i]
        norm1 += vec1[i] * vec1[i]
        norm2 += vec2[i] * vec2[i]
    }

    if norm1 == 0 || norm2 == 0 {
        return 0
    }

    return dotProduct / (math.Sqrt(norm1) * math.Sqrt(norm2))
}

// NormaliseVector normalizes a vector so all values sum to 1
func NormaliseVector(vector []float64) []float64 {
    var sum float64
    for _, v := range vector {
        sum += v
    }

    for i, v := range vector {
        vector[i] = v / sum
    }
    return vector
}

// TrigramVector creates a trigram frequency vector
func TrigramVector(input []byte) ([]float64, error) {
    vector := make([]float64, 26*26*26)
    for i := 0; i < len(input)-2; i++ {
        first := int(input[i]) % 26
        second := int(input[i+1]) % 26
        third := int(input[i+2]) % 26
        index := first*26*26 + second*26 + third
        vector[index]++
    }

    return NormaliseVector(vector), nil
}

// Mod26Vector creates a bigram frequency vector
func Mod26Vector(input []byte) ([]float64, error) {
    vector := make([]float64, 26*26)
    for i := 0; i < len(input)-1; i++ {
        first := int(input[i]) % 26
        second := int(input[i+1]) % 26
        index := first*26 + second
        vector[index]++
    }

    return NormaliseVector(vector), nil
}