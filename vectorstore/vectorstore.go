package vectorstore

import (
    "encoding/binary"
    "fmt"
    "math"
    "path/filepath"
    "sync"
	"sort"

    "github.com/donomii/ensemblekv"
)

// Vector represents a generic vector with its document ID
type Vector struct {
    DocID string
    Vec   []float64
}

// VectorStores manages separate KV stores for different vector types
type VectorStores struct {
    stores map[string]ensemblekv.KvLike
    mutex  sync.RWMutex
}

// Store types
const (
    ContentBigrams  = "content_bigrams"
    PathBigrams     = "path_bigrams"
    ContentTrigrams = "content_trigrams"
    PathTrigrams    = "path_trigrams"
    AIVectors       = "ai_vectors"
)

// NewVectorStores creates and initializes vector stores
func NewVectorStores(basePath string) (*VectorStores, error) {
    vs := &VectorStores{
        stores: make(map[string]ensemblekv.KvLike),
    }

    storeTypes := []string{
        ContentBigrams,
        PathBigrams,
        ContentTrigrams,
        PathTrigrams,
        AIVectors,
    }

    for _, storeType := range storeTypes {
        store, err := ensemblekv.NewExtentKeyValueStore(filepath.Join(basePath, storeType), 8)
        if err != nil {
            // FIXME vs.Close() // Clean up any stores already created
            return nil, fmt.Errorf("failed to create %s store: %v", storeType, err)
        }
        vs.stores[storeType] = store
    }

    return vs, nil
}

// StoreVector stores a single vector
func (vs *VectorStores) StoreVector(storeType string, docID string, vec []float64) error {
    vs.mutex.Lock()
    defer vs.mutex.Unlock()

    store, ok := vs.stores[storeType]
    if !ok {
        return fmt.Errorf("unknown store type: %s", storeType)
    }

    if len(vec) > 0 {
        return store.Put(vectorToBytes(vec), []byte(docID))
    }
    return nil
}

// searchResult holds a document ID and its similarity score
type SearchResult struct {
    DocID string
    Score float64
}


// Search performs similarity search on a single vector type
func (vs *VectorStores) Search(storeType string, queryVec, negativeQueryVec []float64, limit int) ([]SearchResult, error) {
    vs.mutex.RLock()
    defer vs.mutex.RUnlock()

    store, ok := vs.stores[storeType]
    if !ok {
        return nil, fmt.Errorf("unknown store type: %s", storeType)
    }

    results := make(map[string]float64)
    
    store.MapFunc(func(vecBytes []byte, docIDBytes []byte) error {
        vec := bytesToVector64(vecBytes)
        similarity := cosineSimilarity(queryVec, vec)

        // Apply negative query vector
        if len(negativeQueryVec) > 0 {
            similarity -= cosineSimilarity(negativeQueryVec, vec)
        }
        
        docID := string(docIDBytes)
        results[docID] += similarity
        
        return nil
    })

    // Convert results map to sorted slice
    sorted := make([]SearchResult, 0, len(results))
    for docID, score := range results {
        sorted = append(sorted, SearchResult{docID, score})
    }

    // Sort by score descending
    sort.Slice(sorted, func(i, j int) bool {
        return sorted[i].Score > sorted[j].Score
    })

    // Limit results
    if len(sorted) > limit {
        sorted = sorted[:limit]
    }

    return sorted, nil
}

// bytesToVector64 converts bytes back to float64 slice
func bytesToVector64(bytes []byte) []float64 {
    vec := make([]float64, len(bytes)/8)
    for i := range vec {
        vec[i] = math.Float64frombits(binary.LittleEndian.Uint64(bytes[i*8:]))
    }
    return vec
}

// vectorToBytes converts a float32/float64 slice to bytes
func vectorToBytes(vec interface{}) []byte {
    switch v := vec.(type) {
    case []float32:
        bytes := make([]byte, len(v)*4)
        for i, f := range v {
            binary.LittleEndian.PutUint32(bytes[i*4:], math.Float32bits(f))
        }
        return bytes
    case []float64:
        bytes := make([]byte, len(v)*8)
        for i, f := range v {
            binary.LittleEndian.PutUint64(bytes[i*8:], math.Float64bits(f))
        }
        return bytes
    default:
        return nil
    }
}

// Close cleans up all stores
func (vs *VectorStores) Close() error {
    vs.mutex.Lock()
    defer vs.mutex.Unlock()

    for _, store := range vs.stores {
        if err := store.Flush(); err != nil {
            return fmt.Errorf("failed to flush store: %v", err) 
        }
        if err := store.Close(); err != nil {
            return fmt.Errorf("failed to close store: %v", err)
        }
    }

    return nil
}